# REF
### Terminology
'ref' stands for 'reference', within this project 'relationship' and
'reference' are interchangeable.
### Variables
'ref_excludes' disabling ref-enabled entity type even it declares
'ref' in hook_entity_info().
```
  variable_get('ref_excludes', array());
```
### Reserved keywords
Source table alias is always 's'
```
ref('node', $node)->find()->condition('s.id', $id);
```
Attribute name is dot notation
```
ref('node', $node)->find('user', $user)
  ->attr('foo.bar.foo.foo', 'bar')->result();
```
### Usage
* Create relationship of 2 entities with schema-less data
```
ref('node', $node)->create('user', $user, array(
  'foo' => array(
    'bar' => array(
      'foo' => array(
        'foo' => 'bar',
        'bar' => 'foo'
      )
    )
  )
))->save();
```
* Find relationships by an attribute
```
ref('node', $node)->find('user', $user)
  ->attr('foo.bar.foo.foo', 'bar')->result();
```
* Find relationship with specific ref id
```
ref('node', $node)->find()->condition('s.id', $id)->result();
```
* Find relationships with multiple attributes
```
ref('user', $user)->find()->attr('foo.bar', 10, '>')
  ->attr('bar.foo.postcode', 4000)->result();
```
* Remove all relationships of a node
```
ref('node', $node)->find()->remove();
```
* Remove relationships with explicit targeted entity type and attribute
```
ref('user', $user)->find('node')->attr('foo.bar', 10, '>')->remove();
```
* Update one single relationship attribute
```
$ref = ref_load('node', $ref_id);
if (is_object($ref)) {
  $ref->setAttr(array(
    'address' => array(
      'city' => 'Brisbane'
    )
  ))->save();
}
```
* Update all relationship attributes
```
$ref = ref('node', $node)->find()->condition('s.id', $id)->result();
if (is_object($ref)) {
  $ref->setAttrs($attrs)->save();
}
```
