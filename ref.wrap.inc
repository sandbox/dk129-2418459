<?php

/**
 * @file
 * Ref wrapper class definition.
 */

/**
 * Class RefWrap.
 */
class RefWrap {

  private $sourceId;
  private $sourceEntityType;
  private $sourceEntityBundle;
  private $sourceRefTable;
  private $sourceAttrTable;
  private $find;

  /**
   * Constructor.
   */
  public function __construct($entity_type, $entity) {
    $info = entity_get_info($entity_type);
    $this->sourceEntityType = $entity_type;

    $bundle = $entity_type;
    if (isset($info['entity keys']['bundle'])) {
      $bundle_key = $info['entity keys']['bundle'];
      $bundle = isset($entity->$bundle_key) ? $entity->$bundle_key : $bundle;
    }

    $this->sourceEntityBundle = $bundle;
    $this->sourceRefTable = $info['ref']['base'];
    $this->sourceAttrTable = $info['ref']['attr'];

    $id_key = $info['entity keys']['id'];
    $this->sourceId = $entity->$id_key;

    return $this;
  }

  /**
   * Create a Ref object.
   */
  public function create($entity_type, $entity, $values = array(), $symmetrical = FALSE) {
    $info = entity_get_info($entity_type);

    $bundle = $entity_type;
    if (isset($info['entity keys']['bundle'])) {
      $bundle_key = $info['entity keys']['bundle'];
      $bundle = isset($entity->$bundle_key) ? $entity->$bundle_key : $bundle;
    }

    $id_key = $info['entity keys']['id'];
    $ref_id = $entity->$id_key;

    // @todo refactor this
    $props = array(
      'source_entity_type' => $this->sourceEntityType,
      'source_entity_bundle' => $this->sourceEntityBundle,
      'source_ref_table' => $this->sourceRefTable,
      'source_attr_table' => $this->sourceAttrTable,
      'source_id' => $this->sourceId,
      'ref_entity_type' => $entity_type,
      'ref_entity_bundle' => $bundle,
      'ref_id' => $ref_id,
    );
    $ref = new Ref();
    $ref->init($props);
    $ref->setAttrs($values);
    if ($symmetrical) {
      $ref->createSymmetry();
    }
    return $ref;
  }

  /**
   * Find ref with source entity.
   */
  public function find($entity_type = NULL, $entity = NULL) {
    $options = array(
      'source_ref_table' => $this->sourceRefTable,
      'source_attr_table' => $this->sourceAttrTable,
      'source_entity_type' => $this->sourceEntityType,
    );
    $this->find = new RefFind($this->sourceRefTable, 's', $options);
    $this->find->fields('s');
    $this->find->leftJoin($this->sourceAttrTable, 'a', 'a.base_id = s.id');
    $this->find->condition('s.source_id', $this->sourceId);

    if (!is_null($entity_type)) {
      $this->find->condition('s.ref_entity_type', $entity_type);

      $info = entity_get_info($entity_type);
      $bundle = $entity_type;

      if (isset($info['entity keys']['bundle']) && !is_null($entity)) {
        $bundle_key = $info['entity keys']['bundle'];
        $bundle = isset($entity->$bundle_key) ? $entity->$bundle_key : $bundle;

        $id_key = $info['entity keys']['id'];
        $ref_id = $entity->$id_key;

        $this->find->condition('s.ref_id', $ref_id);
      }

      $this->find->condition('s.ref_entity_bundle', $bundle);
    }
    return $this->find;
  }

}
