<?php

/**
 * @file
 * Ref custom views relationship handler.
 */

/**
 * Class RefHandlerRelationship.
 */
class RefHandlerRelationship extends views_handler_relationship {

  private $refTable;

  const REF_FIELD = 'source_id';

  /**
   * Override parent init.
   */
  public function init(&$view, &$options) {
    parent::init($view, $options);
    $this->refTable = $this->definition['ref table'];
  }

  /**
   * Called to implement a relationship in a query.
   */
  public function query() {
    $this->ensure_my_table();

    $left_alias = $this->refTable . '_' . $this->table;

    $def = array();
    $def['table'] = $this->refTable;
    $def['field'] = self::REF_FIELD;
    $def['left_table'] = $this->table_alias;
    $def['left_field'] = $this->real_field;
    $def['type'] = 'LEFT';
    $def['extra'] = array(
      array(
        'table' => 'node_ref_node',
        'field' => 'ref_entity_type',
        'value' => 'user',
      ),
    );
    $join = new views_join();
    $join->definition = $def;
    $join->construct();
    $join->adjusted = TRUE;
    $this->query->add_relationship($left_alias, $join, $this->refTable, $this->relationship);

    // Figure out what base table this relationship brings to the party.
    $table_data = views_fetch_data($this->definition['base']);
    $base_field = empty($this->definition['base field']) ? $table_data['table']['base']['field'] : $this->definition['base field'];

    $def = $this->definition;
    $def['table'] = $this->definition['base'];
    $def['field'] = $base_field;
    $def['left_table'] = $left_alias;
    $def['left_field'] = 'ref_id';
    // @todo Fix this hard code joining type.
    $def['type'] = 'INNER';
    if (!empty($this->definition['extra'])) {
      $def['extra'] = $this->definition['extra'];
    }

    if (!empty($def['join_handler']) && class_exists($def['join_handler'])) {
      $join = new $def['join_handler']();
    }
    else {
      $join = new views_join();
    }

    $join->definition = $def;
    $join->options = $this->options;
    $join->construct();
    $join->adjusted = TRUE;

    // Use a short alias for this.
    $alias = $def['table'] . '_' . $left_alias;
    $this->alias = $this->query->add_relationship($alias, $join, $this->definition['base'], $this->relationship);

    // Add access tags if the base table provide it.
    if (empty($this->query->options['disable_sql_rewrite']) && isset($table_data['table']['base']['access query tag'])) {
      $access_tag = $table_data['table']['base']['access query tag'];
      $this->query->add_tag($access_tag);
    }
  }

}
