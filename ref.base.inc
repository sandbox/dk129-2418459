<?php

/**
 * @file
 * Ref base and ref group class definition.
 */

/**
 * Class Ref.
 */
class Ref {

  public $id;
  public $sourceEntityType;
  public $sourceEntityBundle;
  public $sourceRefTable;
  public $sourceAttrTable;
  public $sourceId;
  public $refEntityType;
  public $refEntityBundle;
  public $refId;
  public $archived;
  public $attr;

  /**
   * Initialize object properties.
   */
  public function init($props) {
    $maps = array(
      'id' => 'id',
      'source_id' => 'sourceId',
      'ref_entity_type' => 'refEntityType',
      'ref_entity_bundle' => 'refEntityBundle',
      'ref_id' => 'refId',
      'archived' => 'archived',
      'source_entity_type' => 'sourceEntityType',
      'source_entity_bundle' => 'sourceEntityBundle',
      'source_ref_table' => 'sourceRefTable',
      'source_attr_table' => 'sourceAttrTable',
    );
    $intersect = array_intersect_key($props, $maps);
    foreach ($intersect as $key => $val) {
      $this->{$maps[$key]} = $val;
    }
  }

  /**
   * Load ref attributes and structure them in array.
   */
  public function loadAttrs() {
    $select = db_select($this->sourceAttrTable, 'a');
    $select->fields('a');
    $select->condition('a.base_id', $this->id);
    $attrs = $select->execute()->fetchAll();
    // @todo investigate efficiency of the loop below
    foreach ($attrs as $each) {
      $this->hierarchizeNotationArray($this->attr, $each->attr, $each->val);
    }
  }

  /**
   * Get ref attribute value by attribute name.
   *
   * @param string $key
   *    Attribute name in dot notation format.
   * @param mixed $default
   *    Default value if there's no attribute.
   *
   * @return mixed
   *    Mixed data type.
   */
  public function getAttr($key, $default = NULL) {
    return $this->get($this->attr, $key, $default);
  }

  /**
   * Set value for ref attribute.
   *
   * @param string $key
   *    Attribute name in dot notation format.
   * @param mixed $value
   *    Attribute value.
   *
   * @return this
   *    This.
   */
  public function setAttr($key, $value) {
    $this->set($this->attr, $key, $value);
    return $this;
  }

  /**
   * Override ref attributes.
   *
   * @param array $attrs
   *    Attributes.
   *
   * @return this
   *    This.
   */
  public function setAttrs(array $attrs) {
    $this->attr = $attrs;
    return $this;
  }

  /**
   * Helper function to get value in array with dot notation key.
   *
   * @param mixed $array
   *    Data array.
   * @param string $key
   *    Dot notation key.
   * @param mixed $default
   *    Default value.
   *
   * @return mixed
   *    Mixed data type.
   *
   * @see https://raw.githubusercontent.com/laravel/framework/master/src/Illuminate/Support/Arr.php
   */
  public function get($array, $key, $default = NULL) {
    if (is_null($key)) {
      return $array;
    }
    if (isset($array[$key])) {
      return $array[$key];
    }
    foreach (explode('.', $key) as $segment) {
      if (!is_array($array) || !array_key_exists($segment, $array)) {
        return $default;
      }
      $array = $array[$segment];
    }
    return $array;
  }

  /**
   * Helper function to set member value with dot notation key.
   *
   * @param mixed $array
   *    Data array to be altered.
   * @param string $key
   *    Dot notation key.
   * @param mixed $value
   *    Value.
   *
   * @return mixed
   *    Mixed data type.
   *
   * @see https://raw.githubusercontent.com/laravel/framework/master/src/Illuminate/Support/Arr.php
   */
  public function set(&$array, $key, $value) {
    if (is_null($key)) {
      return $array = $value;
    }
    $keys = explode('.', $key);
    while (count($keys) > 1) {
      $key = array_shift($keys);
      if (!isset($array[$key]) || !is_array($array[$key])) {
        $array[$key] = array();
      }
      $array =& $array[$key];
    }
    $array[array_shift($keys)] = $value;
    return $array;
  }

  /**
   * Load source entity.
   */
  public function getSourceEntity() {
    return entity_load_single($this->sourceEntityType, $this->sourceId);
  }

  /**
   * Load ref entity.
   */
  public function getRefEntity() {
    return entity_load_single($this->refEntityType, $this->refId);
  }

  /**
   * Delete all attributes.
   *
   * @param bool $reset
   *    Whether reset ref attributes.
   */
  public function flushAttrs($reset = FALSE) {
    db_delete($this->sourceAttrTable)->condition('base_id', $this->id)->execute();
    if ($reset) {
      $this->attr = array();
    }
  }

  /**
   * Store attribute value to db.
   */
  public function saveAttrs() {
    $flat = $this->notateMultidimensionArray($this->attr);
    foreach ($flat as $k => $v) {
      // @todo find more efficient way for below update
      db_merge($this->sourceAttrTable)->key(array(
        'base_id' => $this->id,
        'attr' => strtolower($k),
      ))->fields(array(
        'val' => $v,
      ))->execute();
    }
  }

  /**
   * Save existed or new ref.
   *
   * @param bool $flush
   *    Whether flush attributes and re-save or not.
   *
   * @throws \Exception
   *    Database transaction exception.
   */
  public function save($flush = FALSE) {
    // Update archived prop and ref attributes.
    if (isset($this->id)) {
      db_update($this->sourceRefTable)->fields(array(
        'archived' => $this->archived,
      ))->condition('id', $this->id)->execute();
      if ($flush) {
        $this->flushAttrs();
        $this->saveAttrs();
      }
    }
    // Save a new ref record.
    else {
      $this->id = db_insert($this->sourceRefTable)->fields(array(
        'source_id' => $this->sourceId,
        'ref_entity_type' => $this->refEntityType,
        'ref_entity_bundle' => $this->refEntityBundle,
        'ref_id' => $this->refId,
      ))->execute();
      // Save attributes.
      $this->saveAttrs();
      // Save symmetrical ref.
      if (isset($this->symmetry)) {
        $this->symmetry->save();
        $group = new RefGroup();
        $group->symmetrical = TRUE;
        $group->addMember($this)->addMember($this->symmetry)->save();
      }
    }
  }

  /**
   * Delete itself completely.
   */
  public function delete() {
    $this->flushAttrs();
    db_delete($this->sourceRefTable)->condition('id', $this->id)->execute();
  }

  /**
   * Archive itself without deletion.
   */
  public function archive() {
    $this->archived = 1;
    $this->save();
  }

  /**
   * Create symmetrical ref.
   */
  public function createSymmetry() {
    $entity = entity_load_single($this->refEntityType, $this->refId);
    $info = entity_get_info($this->refEntityType);
    $id_key = $info['entity keys']['id'];
    $target_id = $entity->$id_key;

    $bundle = $this->refEntityType;
    if (isset($info['entity keys']['bundle'])) {
      $bundle_key = $info['entity keys']['bundle'];
      $bundle = isset($entity->$bundle_key) ? $entity->$bundle_key : $bundle;
    }

    if (isset($info['ref'])) {
      // @todo Refactor this.
      $props = array(
        'source_id' => $target_id,
        'ref_entity_type' => $this->sourceEntityType,
        'ref_entity_bundle' => $this->sourceEntityBundle,
        'ref_id' => $this->sourceId,
        'source_entity_type' => $this->refEntityType,
        'source_entity_bundle' => $bundle,
        'source_ref_table' => $info['ref']['base'],
        'source_attr_table' => $info['ref']['attr'],
      );
      $ref = new Ref();
      $ref->init($props);
      $ref->setAttrs($this->attr);

      // @todo Should fix this hidden prop, make it visible.
      $this->symmetry = $ref;
    }
  }

  /**
   * Flatten multidimensional array to 2 dimensional array with notation key.
   *
   * @param mixed $array
   *    Data array.
   *
   * @return array
   *    2 dimensional array.
   */
  public function notateMultidimensionArray($array) {
    $iterator = new RecursiveIteratorIterator(new RecursiveArrayIterator($array));
    $result = array();
    foreach ($iterator as $leaf_value) {
      $keys = array();
      foreach (range(0, $iterator->getDepth()) as $depth) {
        $keys[] = $iterator->getSubIterator($depth)->key();
      }
      $result[implode('.', $keys)] = $leaf_value;
    }
    return $result;
  }

  /**
   * Convert 2 dimensional array with notation key to multidimensional array.
   *
   * @param mixed $arr
   *    Array to be altered.
   * @param string $path
   *    Dot notation key.
   * @param mixed $value
   *    Value.
   */
  public function hierarchizeNotationArray(&$arr, $path, $value) {
    $keys = explode('.', $path);

    while ($key = array_shift($keys)) {
      $arr = &$arr[$key];
    }

    $arr = $value;
  }

}

/**
 * Class RefGroup.
 */
class RefGroup {

  public $id;
  public $identifier;
  public $archived;
  public $symmetrical;
  public $members;

  /**
   * Load members in constructor.
   */
  public function __construct() {
    $this->loadMembers();
  }

  /**
   * Load members of the group.
   */
  public function loadMembers() {
    // Reset members.
    $this->members = array();
    $result = db_select('ref_member', 'm')->fields('m')
      ->condition('m.group_id', $this->id)->execute()->fetchAll();
    foreach ($result as $each) {
      $this->members[$each->sourceRefTable . '.' . $each->source_ref_id]
        = ref_load($each->sourceEntityType, $each->source_ref_id);
    }
  }

  /**
   * Get other refs in the group.
   */
  public function getOther(Ref $ref) {
    return array_intersect_key($this->members,
      array_flip(array($ref->sourceRefTable . '.' . $ref->id)));
  }

  /**
   * Find children based on source entity type of the child.
   *
   * @param string $source_entity_type
   *    Source entity type.
   *
   * @usage $group->find('node')->condition('s.id', $id)->result();
   *
   * @return \RefFind
   *    Ref find wrapper.
   */
  public function findMembers($source_entity_type) {
    $info = entity_get_info($source_entity_type);
    $options = array(
      'source_ref_table' => $info['ref']['base'],
      'source_attr_table' => $info['ref']['attr'],
      'source_entity_type' => $source_entity_type,
    );
    $find = new RefFind($options['source_ref_table'], 's', $options);
    $find->fields('s');
    $find->join('ref_member', 'm', 'm.source_ref_id = s.id AND m.source_ref_table = :name',
      array(':name' => $options['source_ref_table']));
    $find->leftJoin($options['source_attr_table'], 'a', 'a.base_id = s.id');
    $find->condition('m.group_id', $this->id);
    return $find;
  }

  /**
   * Save existed group or create a new group.
   *
   * @param bool $flush
   *    Whether flush group member and re-save.
   */
  public function save($flush = FALSE) {
    if (isset($this->id)) {
      db_update('ref_group')->fields(array(
        'identifier' => $this->identifier,
        'archived' => $this->archived,
        'symmetrical' => $this->symmetrical,
      ))->condition('id', $this->id)->execute();
      if ($flush) {
        $this->flushMembers();
        $this->saveMembers();
      }
    }
    // Save a new ref group record.
    else {
      $this->id = db_insert('ref_group')->fields(array(
        'identifier' => $this->identifier,
        'archived' => (isset($this->archived) && $this->archived) ? 1 : 0,
        'symmetrical' => (isset($this->symmetrical) && $this->symmetrical) ? 1 : 0,
      ))->execute();
      // Save members.
      $this->saveMembers();
    }
  }

  /**
   * Flush members.
   *
   * @param bool $reset
   *    Whether reset members prop.
   */
  public function flushMembers($reset = FALSE) {
    db_delete('ref_member')->condition('group_id', $this->id)->execute();
    if ($reset) {
      $this->members = array();
    }
  }

  /**
   * Save members to db.
   */
  public function saveMembers() {
    if (!empty($this->members)) {
      $insert = db_insert('ref_member')->fields(array(
        'group_id',
        'source_entity_type',
        'source_ref_table',
        'source_ref_id',
      ));;
      foreach ($this->members as $member) {
        $insert->values(array(
          'group_id' => $this->id,
          'source_entity_type' => $member->source_entity_type,
          'source_ref_table' => $member->sourceRefTable,
          'source_ref_id' => $member->id,
        ));
      }
      $insert->execute();
    }
  }

  /**
   * Add member to group.
   */
  public function addMember(Ref $ref) {
    $this->members[$ref->sourceRefTable . '.' . $ref->id] = $ref;
    return $this;
  }

  /**
   * Delete itself completely.
   */
  public function delete() {
    $this->flushMembers();
    return db_delete('ref_group')->condition('id', $this->id)->execute();
  }

  /**
   * Archive itself without deletion.
   */
  public function archive() {
    $this->archived = 1;
    $this->save();
  }

}
