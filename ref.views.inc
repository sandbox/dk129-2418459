<?php

/**
 * @file
 * Views implementation.
 */

/**
 * Implements hook_views_data_alter().
 */
function ref_views_data_alter(&$data) {
  $data['node']['ref_user']['relationship'] = array(
    'title' => t('Refs to user'),
    'help' => t('Node with refs to user.'),
    // 'base' is the target table.
    'base' => 'users',
    // 'base field' is the key of target table.
    'base field' => 'uid',
    // 'relationship field' is the key of source table.
    'relationship field' => 'nid',
    'label' => t('ref'),
    'handler' => 'RefHandlerRelationship',
    // 'ref table' is the source ref table.
    'ref table' => 'node_ref',
  );
}
