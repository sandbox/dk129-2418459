<?php

/**
 * @file
 * Ref Find wrapper class definition.
 */

/**
 * Class RefFind.
 */
class RefFind extends SelectQuery {

  private $sourceRefTable;
  private $sourceAttrTable;
  private $attrCondition;
  private $sourceEntityType;

  /**
   * Constructor.
   *
   * @param string $table
   *    The table name.
   * @param mixed $alias
   *    The table alias.
   * @param array $options
   *    The options to pass in db connection.
   */
  public function __construct($table, $alias = NULL, array $options = array()) {
    $connection = $this->getConnection($options);
    parent::__construct($table, $alias, $connection, $options);

    if (isset($options['source_ref_table'])) {
      // Normally this is $table.
      $this->sourceRefTable = $options['source_ref_table'];
    }

    if (isset($options['source_attr_table'])) {
      $this->sourceAttrTable = $options['source_attr_table'];
    }

    if (isset($options['source_entity_type'])) {
      $this->sourceEntityType = $options['source_entity_type'];
    }
  }

  /**
   * Get database connection.
   *
   * @param array $options
   *    Options as target information.
   *
   * @return DatabaseConnection
   *    Datebase connection.
   */
  public function getConnection($options = array()) {
    if (empty($options['target'])) {
      $options['target'] = 'default';
    }
    return Database::getConnection($options['target']);
  }

  /**
   * Shortcut of execute() and fetchAll().
   */
  public function result() {
    if (isset($this->attrCondition)) {
      $this->condition($this->attrCondition);
    }
    $result = $this->execute()->fetchAll(PDO::FETCH_ASSOC);
    $refs = array();
    // @todo Refactor below.
    foreach ($result as $record) {
      $ref = new Ref();
      $ref->init($record);
      $ref->sourceEntityType = $this->sourceEntityType;
      $ref->sourceRefTable = $this->sourceRefTable;
      $ref->sourceAttrTable = $this->sourceAttrTable;
      $ref->loadAttrs();
      $refs[$ref->id] = $ref;
    }
    return (count($refs) == 1) ? reset($refs) : $refs;
  }

  /**
   * Remove any result that matches.
   */
  public function remove() {
    if (isset($this->attrCondition)) {
      $this->condition($this->attrCondition);
    }
    $result = $this->execute()->fetchAll(PDO::FETCH_ASSOC);
    // @todo Refactor below.
    foreach ($result as $record) {
      $ref = new Ref();
      $ref->init($record);
      $ref->sourceRefTable = $this->sourceRefTable;
      $ref->archive();
    }
  }

  /**
   * Make query condition on attribute value.
   *
   * @param string $attr
   *    Dot notation attribute name.
   * @param mixed $val
   *    Attribute value to compare against.
   * @param mixed $operator
   *    Operator of the condition.
   *
   * @return this
   *    This.
   */
  public function attr($attr, $val = NULL, $operator = NULL) {
    if (!isset($this->attrCondition)) {
      $this->attrCondition = new DatabaseCondition('OR');
    }
    if (!isset($operator)) {
      $operator = '=';
    }

    $and = db_and();
    $and->condition('a.attr', '%' . db_like($attr) . '%', 'LIKE');

    if (is_array($val)) {
      $and->condition('a.val', $val);
    }
    elseif (is_scalar($val)) {
      if (is_numeric($val)) {
        // @todo make sure $operator is validated
        $and->where("a.val $operator $val");
      }
      else {
        $and->condition('a.val', $val, $operator);
      }
    }
    $this->attrCondition->condition($and);

    return $this;
  }

}

/**
 * Class RefGroupFind.
 */
class RefGroupFind extends SelectQuery {

  private $memberAttr;
  private $memberProp;
  private $aliases;

  /**
   * Constructor.
   *
   * @param array $options
   *    Options to pass in db connection.
   */
  public function __construct(array $options = array()) {
    $connection = $this->getConnection($options);
    parent::__construct('ref_group', 'g', $connection, $options);
    $this->fields('g');
    $this->distinct();
    $this->join('ref_member', 'm', 'm.group_id = g.id');
  }

  /**
   * Get the db connection.
   *
   * @param array $options
   *    Options to pass in db connection.
   *
   * @return \DatabaseConnection
   *    Db connection.
   */
  public function getConnection(array $options = array()) {
    if (empty($options['target'])) {
      $options['target'] = 'default';
    }
    return Database::getConnection($options['target']);
  }

  /**
   * Add in the source entity type before query.
   *
   * @param string $entity_type
   *    Entity type to query on.
   *
   * @return this
   *    This.
   */
  public function addEntityType($entity_type) {
    $info = entity_get_info($entity_type);

    $table = $info['ref']['base'];
    $attr_table = $info['ref']['attr'];

    $this->join($table, $table, $table . '.id = m.source_ref_id');
    $this->join($attr_table, $attr_table, $attr_table . '.base_id = m.source_ref_id');

    $this->aliases[$entity_type] = array(
      'table' => $table,
      'attr_table' => $attr_table,
    );

    return $this;
  }

  /**
   * Query on ref member attribute.
   *
   * @param string $entity_type
   *    Entity type of member.
   * @param string $attr
   *    Dot notation attribute name.
   * @param mixed $val
   *    Attribute value to compare against.
   * @param mixed $operator
   *    Condition operator.
   *
   * @return this
   *    This.
   */
  public function memberAttr($entity_type, $attr, $val = NULL, $operator = NULL) {
    $alias = $this->aliases[$entity_type]['attr_table'];

    if (!isset($this->memberAttr)) {
      $this->memberAttr = new DatabaseCondition('OR');
    }
    if (!isset($operator)) {
      $operator = '=';
    }

    $and = db_and();
    $and->condition($alias . '.attr', '%' . db_like($attr) . '%', 'LIKE');

    if (is_array($val)) {
      $and->condition($alias . '.val', $val);
    }
    elseif (is_scalar($val)) {
      if (is_numeric($val)) {
        // @todo Make sure $operator is sanitized.
        $and->where("$alias.val $operator $val");
      }
      else {
        $and->condition($alias . '.val', $val, $operator);
      }
    }
    $this->memberAttr->condition($and);

    return $this;
  }

  /**
   * Query on ref member property.
   *
   * @param string $entity_type
   *    Entity type of member.
   * @param string $prop
   *    Property name.
   * @param mixed $val
   *    Property value to compare against.
   * @param mixed $operator
   *    Condition operator.
   *
   * @return this
   *    This.
   */
  public function memberProp($entity_type, $prop, $val = NULL, $operator = NULL) {
    $alias = $this->aliases[$entity_type]['table'];

    if (!isset($this->memberProp)) {
      $this->memberProp = new DatabaseCondition('AND');
    }
    if (!isset($operator)) {
      $operator = '=';
    }

    $this->memberProp->condition($alias . '.' . $prop, $val, $operator);

    return $this;
  }

  /**
   * Shortcut of execute() and fetchAll().
   */
  public function result() {
    if (isset($this->member_prop)) {
      $this->condition($this->member_prop);
    }
    if (isset($this->memberAttr)) {
      $this->condition($this->memberAttr);
    }
    $result = $this->execute()->fetchAll(PDO::FETCH_CLASS, 'RefGroup');
    return (count($result) == 1) ? reset($result) : $result;
  }

  /**
   * Remove any result that match.
   */
  public function remove() {
    if (isset($this->member_prop)) {
      $this->condition($this->member_prop);
    }
    if (isset($this->memberAttr)) {
      $this->condition($this->memberAttr);
    }
    $result = $this->execute()->fetchAll(PDO::FETCH_CLASS, 'RefGroup');
    foreach ($result as $ref_group) {
      $ref_group->archive();
    }
  }

}
